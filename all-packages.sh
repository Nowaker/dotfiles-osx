#!/bin/bash

echo "Installing all packages in priority order..."

echo "Starting P1 (highest priority) packages installation..."
./p1-packages.sh

echo "Starting P2 (medium priority) packages installation..."
./p2-packages.sh

echo "Starting P3 (lowest priority) packages installation..."
./p3-packages.sh

echo "All packages installation completed!"