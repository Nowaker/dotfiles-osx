#!/bin/bash

# Source common functions
source "./_commons.sh"

# Install P3 (lowest priority) packages using the common function
install_packages "p3-packages.txt" "P3 (lowest priority)"