#!/bin/bash

# Source common functions
source "./_commons.sh"

# Install P1 (highest priority) packages using the common function
install_packages "p1-packages.txt" "P1 (highest priority)"