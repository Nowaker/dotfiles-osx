#!/bin/bash

# Source common functions
source "./_commons.sh"

# Install P2 (medium priority) packages using the common function
install_packages "p2-packages.txt" "P2 (medium priority)"