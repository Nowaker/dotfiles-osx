#!/bin/bash

install_packages() {
  local package_file=$1
  local package_type=$2

  echo "Installing ${package_type} packages..."

  while IFS= read -r package; do
    # Skip empty lines and comments
    [[ -z "$package" || "$package" =~ ^# ]] && continue
    
    echo "Installing $package..."
    brew install "$package" || brew install --cask "$package"

    # Special handling for Chromium
    if [ "$package" = "chromium" ]; then
      echo "Applying special handling for Chromium..."
      xattr -cr /Applications/Chromium.app
    fi
  done < "$package_file"

  echo "${package_type} packages installation completed!"
}
